#include "mutex.hpp"

#include <twist/rt/run.hpp>
#include <twist/test/race.hpp>
#include <twist/test/plate.hpp>

#include <chrono>
#include <iostream>

using namespace std::chrono_literals;

////////////////////////////////////////////////////////////////////////////////

using Mutex = PetersonMutex;

void Test() {
  twist::rt::Run([] {
    Mutex mutex;
    twist::test::Plate plate;  // Guarded by mutex

    twist::test::Race race;

    auto contender = [&](size_t index) {
      for (size_t i = 0; i < 100'500; ++i) {
        mutex.Lock(/*me=*/index);
        {
          // Critical section
          plate.Access();
        }
        mutex.Unlock();
      }
    };

    // Spawn threads
    race.AddThread([&] {
      contender(/*index=*/0);
    });
    race.AddThread([&] {
      contender(/*index=*/1);
    });

    race.Run();

    std::cout << "Critical sections: " << plate.AccessCount() << std::endl;
  });
}

int main() {
  Test();
  return 0;
}
